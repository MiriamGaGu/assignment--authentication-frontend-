import React, { Component } from "react";
import {Link} from 'react-router-dom'

export default class Signup extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="mainSection">
            <div className="Logbox">
               <div className="fields">
                <label className="label">Name</label>
                <div className="control">
                    <input className="input" type="text" placeholder="Name"/>
                </div>
             </div>

            <div className="fields">
                <label className="label">Username</label>
                <div className="control has-icons-left has-icons-right">
                    <input className="input " type="text" placeholder="Username" />
                <span className="icon is-small is-left">
                    <i className="fas fa-user"></i>
                </span>
            </div>
          {/* <p className="help ">This username is available</p> */}
        </div>

        <div className="fields">
          <label className="label">Email</label>
          <div className="control has-icons-left has-icons-right">
            <input className="input " type="email" placeholder="Email@"/>
            <span className="icon is-small is-left">
              <i className="fas fa-envelope"></i>
            </span>
          </div>
          {/* <p className="help is-danger">This email is invalid</p> */}
        </div>
        <a class="button is-primary">Log In</a>
        <div className="social">
            <Link to="/" class="button is-link"><i class="fab fa-facebook-f"></i>Facebook</Link>
            <Link to="/" class="button is-danger"><i class="fab fa-google"></i>Google</Link>
            <Link to="/" class="button is-info"><i class="fab fa-twitter"></i>Twitter</Link>
        </div>
            </div>
        </div>
           
</React.Fragment>
    );
  }
}
