import React, { Component } from "react";

import { Link, withRouter } from "react-router-dom";

class Menu extends Component {
  handleLogout = () => {
    const { history } = this.props;

    localStorage.removeItem("token");
    history.push("/");
  }

  render() {
    return (
        <nav className="navbar is-primary">
        <div className="container">
            <div className="navbar-brand">
            <span className="navbar-burger burger" data-target="navMenu">
                <span></span>
                <span></span>
                <span></span>
            </span>
            </div>
            <div id="navMenu" className="navbar-menu">
            <h1 className="navbar-item">App Books</h1>
            <div className="navbar-end">
                
                <Link to="/" className="navbar-item ">Home</Link>
                <Link to="/books" className="navbar-item">Books</Link>
                <Link to="/loans" className="navbar-item">Loans</Link>
                <Link to="/login" className="navbar-item">LogIn</Link>
                <Link to="/signup" className="navbar-item">Signup</Link>
            </div>
            </div>
        </div>
        <li>
                <button className="button is-dark" onClick={ this.handleLogout }>Logout</button>
            </li>
    </nav>
          
       
    );
  }
}

export default withRouter(Menu);
