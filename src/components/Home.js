import React, { Component } from "react";

export default class Home extends Component {
  constructor() {
    super();
    this.state = {
      regions: []
      


    }
  }

  componentDidMount() {
    fetch('https://secure-thicket-75424.herokuapp.com/api/v1/users')
      .then(response => response.json())
      .then(data => {
        
        this.setState({
          regions: data
          
        })
        console.log(data);
      })
  }

  render() {
    return(
      <React.Fragment>
      <article class="message is-warning">
        <div class="message-header">
          <p>There's no place like home</p>
          <button class="delete" aria-label="delete"></button>
        </div>
        <div class="message-body">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. <strong>Pellentesque risus mi</strong>, tempus quis placerat ut, porta nec nulla. Vestibulum rhoncus ac ex sit amet fringilla. Nullam gravida purus diam, et dictum <a>felis venenatis</a> efficitur. Aenean ac <em>eleifend lacus</em>, in mollis lectus. Donec sodales, arcu et sollicitudin porttitor, tortor urna tempor ligula, id porttitor mi magna a neque. Donec dui urna, vehicula et sem eget, facilisis sodales sem.
        </div>
      </article>
      </React.Fragment>
      
    );
  }
}
